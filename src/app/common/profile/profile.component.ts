import {Component, Input, OnInit} from "@angular/core";
import {Profile} from "../../api/profile/profile.data";
import {ProfileApi} from "../../api/profile/profile.api";
import {DomSanitizer} from "@angular/platform-browser";
import {MdDialog} from "@angular/material";
import {ProfileDetailsComponent} from "./profile-details.component";

@Component({
  selector: 'teamw-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  @Input()
  id: number;
  @Input()
  name: string;

  profile: Profile;

  constructor(private profileApi: ProfileApi, private sanitizer:DomSanitizer, private dialog: MdDialog) {
  }

  ngOnInit(): void {
    this.profileApi.getProfile(this.id, this.name).subscribe((profile) => {
      this.profile = <Profile>profile;
      this.profile.portrait.url = this.sanitizer.bypassSecurityTrustStyle(`url(${this.profile.portrait.url})`);
    });
  }

  openProfile() {
    this.dialog.open(ProfileDetailsComponent, {
      data: {
        profile: this.profile
      }
    });
  }
}

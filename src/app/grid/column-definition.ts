import {AgRendererComponent} from "ag-grid-angular";
export interface ColumnDefinition {

  headerName: string;
  field: string;
  cellRendererFramework: AgRendererComponent;
}

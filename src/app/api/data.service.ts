import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {AngularFireDatabase} from "angularfire2/database/database";
import * as firebase from 'firebase/app';
import {Observable} from "rxjs/Observable";
import {AngularFireAuth} from "angularfire2/auth/auth";


@Injectable()
export class DataService {

  user;
  store;

  constructor(db: AngularFireDatabase, private afAuth: AngularFireAuth) {
    Observable.create((observer) => {
      this.doAuth(observer);
    });
    this.store = db;
  }

  private doAuth(observer) {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((result) => {
        this.user = result.user;
        observer.next(result);
      });
  }

}

import {NgModule} from "@angular/core";
import {MdToolbarModule, MdMenuModule, MdButtonModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {TopnavComponent} from "./topnav.component";

@NgModule({
  declarations: [TopnavComponent],
  exports: [TopnavComponent],
  imports: [
    BrowserAnimationsModule,
    MdToolbarModule,
    MdMenuModule,
    MdButtonModule
  ]
})
export class TopnavModule { }

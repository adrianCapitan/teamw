import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {region, locale, apiKey} from "../blizzard";

@Injectable()
export class ProfileApi {

  constructor(private http: Http) {
  }

  getProfile(id: number, name: string) {
    return this.http
      .get(`https://eu.api.battle.net/sc2/profile/${id}/${region}/${name}/?locale=${locale}&apikey=${apiKey}`)
      .map((res) => res.json());
  }
}

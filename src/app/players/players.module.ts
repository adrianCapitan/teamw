import {NgModule} from "@angular/core";
import {PlayersComponent} from "./players.component";
import {GridModule} from "../grid/grid.module";
import {PlayersService} from "./players.service";
import {PlayerApiModule} from "../api/player/player.module";
import {CommonModule} from "@angular/common";
import {MdButtonModule, MdDialogModule} from "@angular/material";
import {DialogFormModule} from "../dialog-form/dialog-form.module";
import {PlayerModule} from "../player/player.module";
import {RaceGridCellModule} from "../common/race/race-grid-cell.module";
import {ProfileModule} from "../common/profile/profile.module";

@NgModule({
  declarations: [PlayersComponent],
  exports: [PlayersComponent],
  providers: [PlayersService],
  imports: [
    DialogFormModule,
    MdDialogModule,
    MdButtonModule,
    GridModule,
    PlayerApiModule,
    PlayerModule,
    CommonModule,
    RaceGridCellModule,
    ProfileModule
  ]
})
export class PlayersModule { }

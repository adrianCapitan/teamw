import {NgModule} from "@angular/core";
import {PlayerApi} from "./player/player.api";
import {DataService} from "./data.service";
import {HttpModule} from "@angular/http";
import {AngularFireModule} from "angularfire2";
import {environment} from "../../environments/environment";
import {AngularFireDatabase} from "angularfire2/database/database";
import {AngularFireAuthModule} from "angularfire2/auth/auth.module";

@NgModule({
  imports: [
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule
  ],
  providers: [
    AngularFireDatabase,
    DataService,
    PlayerApi
  ]
})
export class ApiModule {

}

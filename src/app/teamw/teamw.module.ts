import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LayoutModule } from "../layout/layout.module";
import { TeamWComponent } from "./teamw.component";

import { appRoutes } from "./teamw.route";
import {DashboardModule} from "../dashboard/dashboard.module";
import {PlayersModule} from "../players/players.module";

@NgModule({
  declarations: [TeamWComponent],
  imports: [
    LayoutModule,
    DashboardModule,
    RouterModule.forRoot(appRoutes.map((state) => {
      return state.route;
    })),
    PlayersModule
  ],
  exports: [TeamWComponent]
})
export class TeamWModule {

}

import {Component, Input} from "@angular/core";
import {State} from "../teamw/teamw.route";

@Component({
  selector: 'teamw-navigation-buttons',
  templateUrl: './navigation-buttons.component.html'
})
export class NavigationButtonsComponent {

  @Input()
  states: State[];

}

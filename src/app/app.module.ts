import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TeamWModule } from "./teamw/teamw.module";
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TeamWModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

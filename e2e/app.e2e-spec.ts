import { YieldPage } from './app.po';

describe('yield App', () => {
  let page: YieldPage;

  beforeEach(() => {
    page = new YieldPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

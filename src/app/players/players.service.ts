import {Injectable} from "@angular/core";
import {GridOptions as AgGridOptions, GetContextMenuItemsParams} from "ag-grid/main";
import {GridOptions} from "../grid/grid-options";
import {PlayerApi} from "../api/player/player.api";
import {RaceGridCellComponent} from "../common/race/race-grid-cell.component";
import {ColumnDefinition} from "../grid/column-definition";
import {PlayerComponent} from "../player/player.component";
import {DialogFormComponent} from "../dialog-form/dialog-form.component";
import {MdDialog} from "@angular/material";
import {ProfileGridCellComponent} from "../common/profile/profile-grid-cell.component";

@Injectable()
export class PlayersService {

  constructor(private playerApi: PlayerApi, private dialog: MdDialog) {
  }

  public initGridOptions() {
    let gridOptions = new GridOptions();
    gridOptions.options = <AgGridOptions>{
      getContextMenuItems: (params: GetContextMenuItemsParams) => {
        return this.getContextMenuItems(params);
      }
    };
    gridOptions.columns = <ColumnDefinition[]>[
      {
        headerName: "Profile",
        field: "",
        maxWidth: 105,
        cellRendererFramework: ProfileGridCellComponent
      }, {
        headerName: "Favorite Race",
        field: "favoriteRace",
        maxWidth: 70,
        cellRendererFramework: RaceGridCellComponent,
      }, {
        headerName: "Name",
        field: "name"
      }, {
        headerName: "Nickname",
        field: "nickname"
      }, {
        headerName: "ID",
        field: "id"
      }];
    gridOptions.data = this.playerApi.get({});
    return gridOptions;
  }

  public openForm(item?) {
    this.dialog.open(DialogFormComponent, {
      data: {
        component: PlayerComponent,
        item
      }
    });
  }

  getContextMenuItems(params: GetContextMenuItemsParams) {
    let items = [];

    let deleteItem = {
      name: 'Delete',
      action: () => {
        this.delete(params.api.getSelectedRows());
      }
    };

    let editItem = {
      name: 'Edit',
      action: () => {
        this.edit(params.api.getSelectedRows()[0]);
      }
    };

    if (params.api.getSelectedRows().length > 0) {
      items.push(deleteItem);
    }

    if (params.api.getSelectedRows().length == 1) {
      items.push(editItem);
    }

    return items;
  }

  delete(rows) {
    rows.forEach(row => {
      this.playerApi.delete(row);
    });
  }

  edit(row) {
    this.openForm(row);
  }

}

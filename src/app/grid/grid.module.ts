import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AgGridModule} from "ag-grid-angular";
import {GridComponent} from "./grid.component";

@NgModule({
  declarations: [GridComponent],
  exports: [GridComponent],
  imports: [
    BrowserModule,
    AgGridModule.withComponents([])
  ]
})
export class GridModule {

}

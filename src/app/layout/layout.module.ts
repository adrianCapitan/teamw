import {NgModule} from "@angular/core";
import {LayoutComponent} from "./layout.component";
import {SidenavModule} from "../sidenav/sidenav.module";
import {TopnavModule} from "../topnav/topnav.module";

@NgModule({
  declarations: [LayoutComponent],
  exports: [LayoutComponent],
  imports: [
    SidenavModule,
    TopnavModule
  ]
})
export class LayoutModule { }

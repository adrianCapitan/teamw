import {GridOptions as AgGridOptions} from "ag-grid/main";
import {ColumnDefinition} from "./column-definition";

export class GridOptions {

  public options: AgGridOptions;
  public columns: ColumnDefinition[];
  public getContextMenuItems;
  public data;

}

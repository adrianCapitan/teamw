import {Observable} from "@reactivex/rxjs";

export abstract class Api<T> {

  abstract save(item: T): Observable<T>;
  abstract update(key, item: T): Observable<T>;

}

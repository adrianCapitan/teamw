import {Route} from "@angular/router";

import {DashboardComponent} from "../dashboard/dashboard.component";
import {PlayersComponent} from "../players/players.component";

export interface State {
  route: Route;
  label?: string;
  hideNavigation?: boolean;
}

export const appRoutes: State[] = [
  {
    route: {path: '', redirectTo: '/app', pathMatch: 'full'},
    hideNavigation: true
  },
  {
    route: {path: 'app', component: DashboardComponent},
    label: 'Dashboard'
  },
  {
    route: {path: 'players', component: PlayersComponent},
    label: 'Players'
  }
];

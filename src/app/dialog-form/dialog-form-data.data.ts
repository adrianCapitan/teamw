import {Observable} from "@reactivex/rxjs";

export interface DialogFormData<T> {

  item: T;
  component?;

  save(): Observable<T>;
  discard();
}

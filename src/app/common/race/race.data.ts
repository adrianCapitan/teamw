export interface Race {
  key: string;
  label: string;
  icon: string;
}

import {Component, ViewChild, ComponentFactoryResolver, AfterViewInit, Inject} from "@angular/core";
import {DialogHostDirective} from "./dialog-host.directive";
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";
import {DialogFormData} from "./dialog-form-data.data";
import {FormComponent} from "./form.component";

@Component({
  selector: 'teamw-dialog-form',
  templateUrl: './dialog-form.component.html'
})
export class DialogFormComponent implements AfterViewInit {

  @ViewChild(DialogHostDirective)
  dialogHost: DialogHostDirective;
  componentRef;

  constructor(private _componentFactoryResolver: ComponentFactoryResolver, @Inject(MD_DIALOG_DATA) public dialogData: DialogFormData<any>, private dialogRef: MdDialogRef<DialogFormComponent>) {
  }

  ngAfterViewInit(): void {
    this.loadComponent();
  }

  loadComponent() {
    let componentFactory = this._componentFactoryResolver.resolveComponentFactory(this.dialogData.component);
    let viewContainerRef = this.dialogHost.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
    (<DialogFormData<any>>this.componentRef.instance).item = this.dialogData.item;
  }

  discard() {
    (<DialogFormData<any>>this.componentRef.instance).discard();
    this.dialogRef.close('discard');
  }

  save() {
    if((<FormComponent<any>>this.componentRef.instance).itemForm.valid) {
      (<DialogFormData<any>>this.componentRef.instance).save().subscribe((item)=> {
        this.dialogRef.close(item);
      });
    }
  }
}

export interface Player {

  name: string;
  nickname: string;
  favoriteRace: string;
}

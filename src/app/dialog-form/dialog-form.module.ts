import {NgModule} from "@angular/core";
import {MdDialogModule, MdButtonModule} from "@angular/material";
import {DialogFormComponent} from "./dialog-form.component";
import {DialogHostDirective} from "./dialog-host.directive";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    DialogFormComponent,
    DialogHostDirective
  ],
  imports: [
    CommonModule,
    MdDialogModule,
    MdButtonModule
  ],
  exports: [DialogFormComponent],
  entryComponents: [DialogFormComponent]
})
export class DialogFormModule {

}

import {Injectable} from "@angular/core";
import {appRoutes} from "../teamw/teamw.route";

@Injectable()
export class SidenavService {

  public getNavigationMenus() {
    return appRoutes.filter(state => {
      return !state.hideNavigation;
    });
  }
}

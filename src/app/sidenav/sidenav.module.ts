import {NgModule} from "@angular/core";
import {MdSidenavModule, MdButtonModule} from "@angular/material";
import {RouterModule} from "@angular/router";

import {SidenavComponent} from "./sidenav.component";
import {NavigationButtonsComponent} from "./navigation-buttons.component";
import {CommonModule} from "@angular/common";
import {SidenavService} from "./sidenav.service";

@NgModule({
  declarations: [
    SidenavComponent,
    NavigationButtonsComponent
  ],
  exports: [SidenavComponent],
  providers: [SidenavService],
  imports: [
    CommonModule,
    MdButtonModule,
    MdSidenavModule,
    RouterModule
  ]
})
export class SidenavModule { }

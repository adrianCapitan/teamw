import {Component, Input, OnInit} from "@angular/core";
import {State} from "../teamw/teamw.route";
import {SidenavService} from "./sidenav.service";

@Component({
  selector: 'teamw-sidenav',
  templateUrl: './sidenav.component.html'
})
export class SidenavComponent implements OnInit {

  @Input()
  opened;

  states: State[];

  constructor(private sidenavService: SidenavService) {}

  ngOnInit(): void {
    this.states = this.sidenavService.getNavigationMenus();
  }

}

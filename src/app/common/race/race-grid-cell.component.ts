import {Component} from "@angular/core";
import {AgRendererComponent} from "ag-grid-angular";
import {Race} from "./race.data";
import {ICellRendererParams} from "ag-grid";
import {races} from "./races.constant";

@Component({
  selector: 'teawm-race-grid',
  templateUrl:'./race-grid-cell.component.html'
})
export class RaceGridCellComponent implements AgRendererComponent {

  race: Race;

  agInit(params: ICellRendererParams): void {
    this.race = races.find((race) => {
      return race.key == params.value;
    });
  }

}

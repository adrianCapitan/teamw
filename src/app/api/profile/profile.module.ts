import {NgModule} from "@angular/core";
import {ProfileApi} from "./profile.api";
import {HttpModule} from "@angular/http";

@NgModule({
  imports: [HttpModule],
  providers: [ProfileApi]
})
export class ProfileApiModule {

}

import {OnInit, Input} from "@angular/core";
import {Observable} from "@reactivex/rxjs";
import {FormGroup} from "@angular/forms";

import {DialogFormData} from "./dialog-form-data.data";
import {Api} from "../api/api";

export class FormComponent<T> implements  DialogFormData<T>, OnInit{

  @Input()
  item: T;
  itemForm: FormGroup;

  constructor(private api: Api<T>, itemForm: FormGroup) {
    this.itemForm = itemForm;
  }

  ngOnInit(): void {
    if(this.item) {
      this.itemForm.setValue(this.item);
    }
  }

  save(): Observable<T> {
    if(this.item && this.item['$key']) {
      return this.api.update(this.item['$key'], this.itemForm.getRawValue());
    }
    return this.api.save(this.itemForm.getRawValue());
  }

  discard() {
    return undefined;
  }

}

import {Injectable} from "@angular/core";
import {Api} from "../api";
import {Player} from "./player.data";
import {DataService} from "../data.service";
import {Observable} from "@reactivex/rxjs";

@Injectable()
export class PlayerApi extends Api<Player> {

  constructor(private dataService: DataService) {
    super();
  }

  get(query) {
    return this.dataService.store.list('/players', {
      query: query
    });
  }

  save(player: Player): Observable<Player> {
    return Observable.fromPromise(this.dataService.store.list('/players').push(player));
  }

  update(key, player: Player): Observable<Player> {
    return Observable.fromPromise(this.dataService.store.list('/players').update(key, player));
  }

  delete(player: Player) {
    return Observable.fromPromise(this.dataService.store.list('/players').remove(player['$key']));
  }
}

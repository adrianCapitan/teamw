import {Component, Input} from "@angular/core";
import {Observable} from "@reactivex/rxjs"

@Component({
  selector: 'teamw-topnav',
  templateUrl: './topnav.component.html'
})
export class TopnavComponent {

  @Input()
  observable;

  toggleSidebar() {
    this.observable.subscribe();
  }
}

import {Component} from "@angular/core";
import {Observable} from "@reactivex/rxjs";

@Component({
  selector: 'teamw-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {

  sidenavOpened = false;
  observable = Observable.create(() => {
    this.sidenavOpened = !this.sidenavOpened;
  });
}

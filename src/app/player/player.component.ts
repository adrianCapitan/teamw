import {Component} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";

import {Player} from "./player.data";
import {PlayerApi} from "../api/player/player.api";
import {Api} from "../api/api";
import {FormComponent} from "../dialog-form/form.component";
import {Race} from "../common/race/race.data";
import {races} from "../common/race/races.constant";

@Component({
  selector: 'teamw-player',
  templateUrl: './player.component.html'
})
export class PlayerComponent extends FormComponent<Player> {

  races: Race[] = races;
  currentRace: any = {};

  constructor(api: PlayerApi, private formBuilder: FormBuilder) {
    super(<Api<Player>>api, formBuilder.group({
      name: ['', Validators.required],
      id: ['', Validators.required],
      nickname: ['', Validators.required],
      favoriteRace: ['']
    }));
  }

}

import {Component, OnInit} from "@angular/core";
import {GridOptions} from "../grid/grid-options";
import {PlayersService} from "./players.service";
import {MdDialog} from "@angular/material";
import {DialogFormComponent} from "../dialog-form/dialog-form.component";
import {PlayerComponent} from "../player/player.component";

@Component({
  selector: 'teamw-players',
  templateUrl: './players.component.html'
})
export class PlayersComponent implements OnInit {

  gridOptions: GridOptions;

  constructor(private playersService: PlayersService) {
  }

  ngOnInit(): void {
    this.gridOptions = this.playersService
      .initGridOptions();
  }

  newPlayer(): void {
    this.playersService.openForm();
  }

}

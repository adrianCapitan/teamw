// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBNFowILfjybxg_4jMPPvc-vMSPbkiYhDI',
    authDomain: 'teammanager-b919f.firebaseapp.com',
    databaseURL: 'https://teammanager-b919f.firebaseio.com',
    projectId: 'teammanager-b919f',
    storageBucket: 'teammanager-b919f.appspot.com',
    messagingSenderId: '253402683241'
  }
};

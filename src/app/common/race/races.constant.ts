import {Race} from "./race.data";

export const races : Race[] = [
  {
    key: 'zerg',
    label:'Zerg',
    icon: 'assets/images/zerg.png'
  },
  {
    key: 'terran',
    label: 'Terran',
    icon: 'assets/images/terran.png'
  },
  {
    key: 'protos',
    label: 'Protos',
    icon: 'assets/images/protos.png'
  }
];

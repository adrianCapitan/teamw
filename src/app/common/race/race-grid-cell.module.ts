import {NgModule} from "@angular/core";
import {RaceGridCellComponent} from "./race-grid-cell.component";

@NgModule({
  declarations: [RaceGridCellComponent],
  entryComponents: [RaceGridCellComponent],
  exports: [RaceGridCellComponent]
})
export class RaceGridCellModule {

}

import {Component, Input, OnInit} from "@angular/core";
import {GridOptions as AgGridOptions} from "ag-grid/main";
import {GridOptions} from "./grid-options";
import {ColumnDefinition} from "./column-definition";

@Component({
  selector: 'teamw-grid',
  templateUrl: './grid.component.html'
})
export class GridComponent implements OnInit {

  @Input()
  gridOptions: GridOptions;

  options: AgGridOptions;
  columns: ColumnDefinition[];
  data: [any];


  ngOnInit(): void {
    this.options = this.gridOptions.options;
    this.options.rowSelection = 'single';
    this.options.onGridReady = () => {
      this.gridOptions.options.api.doLayout();
      this.gridOptions.options.api.sizeColumnsToFit();
    };
    this.columns = this.gridOptions.columns;
    this.gridOptions.data.subscribe((items) => {
      this.data = items;
    });
  }


}

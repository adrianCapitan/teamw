import {NgModule} from "@angular/core";
import {PlayerComponent} from "./player.component";
import {MdInputModule, MdOptionModule, MdSelectModule} from "@angular/material";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [PlayerComponent],
  entryComponents: [PlayerComponent],
  exports: [PlayerComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MdInputModule,
    MdSelectModule,
    MdOptionModule
  ]
})
export class PlayerModule {

}

import {NgModule} from "@angular/core";
import {ProfileGridCellComponent} from "./profile-grid-cell.component";
import {ProfileComponent} from "./profile.component";
import {CommonModule} from "@angular/common";
import {ProfileApiModule} from "../../api/profile/profile.module";
import {MdProgressSpinnerModule, MdGridListModule, MdDialogModule} from "@angular/material";
import {ProfileDetailsComponent} from "./profile-details.component";

@NgModule({
  declarations: [
    ProfileGridCellComponent,
    ProfileComponent,
    ProfileDetailsComponent
  ],
  entryComponents: [
    ProfileGridCellComponent,
    ProfileDetailsComponent
  ],
  imports: [
    CommonModule,
    ProfileApiModule,
    MdDialogModule,
    MdGridListModule,
    MdProgressSpinnerModule
  ],
  exports: [
    ProfileGridCellComponent,
    ProfileComponent
  ]
})
export class ProfileModule {

}

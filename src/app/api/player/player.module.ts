import {NgModule} from "@angular/core";
import {PlayerApi} from "./player.api";
import {ApiModule} from "../api.module";

@NgModule({
  providers: [PlayerApi],
  imports: [ApiModule]
})
export class PlayerApiModule {

}

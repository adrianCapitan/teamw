import {Component} from "@angular/core";
import {ICellRendererParams} from "ag-grid";
import {AgRendererComponent} from "ag-grid-angular";

@Component({
  selector: 'teamw-profile-grid-cell',
  templateUrl: './profile-grid-cell.component.html'
})
export class ProfileGridCellComponent implements AgRendererComponent {

  id: number;
  name: string;

  agInit(params: ICellRendererParams): void {
    this.id = params.data.id;
    this.name = params.data.name;
  }

}

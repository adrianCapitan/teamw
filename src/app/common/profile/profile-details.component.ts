import {Component, Input, Inject} from "@angular/core";
import {Profile} from "../../api/profile/profile.data";
import {MD_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'teamw-profile-details',
  templateUrl: './profile-details.component.html'
})
export class ProfileDetailsComponent {

  @Input()
  profile: Profile;

  constructor(@Inject(MD_DIALOG_DATA) profile: Profile) {
    this.profile = profile;
  }
}

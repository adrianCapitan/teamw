export interface Profile {

  season: {

  },
  portrait: {
    x: number,
    y: number,
    w: number,
    h: number,
    url: any
  };
}
